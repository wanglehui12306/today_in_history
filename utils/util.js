//获取当前时间
function formatTime(){
  var date=new Date();
  var month=date.getMonth()+1;
  var day=date.getDay()
  return [month,day]
}
//获取月日
function getnum(num){
  var data=[];
  switch(num){
    case 0:
      for(let i=1;i<=12;i++){
        data.push(i+'月')
      }
      break;
      case 1:
        for(let i=1;i<=31;i++){
          data.push(i+'日')
        }
        break;
        case 2:
          for(let i=2;i<=30;i++){
            data.push(i+'日')
          }
          default:
            for(let i=1;i<=28;i++){
              data.push(i+'日')
            }break
            break;
  }
  return data;
}

module.exports = {
  formatTime: formatTime,
  getnum:getnum
}
