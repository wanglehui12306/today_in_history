//index.js
//获取应用实例
const app = getApp();
var util=require("../../utils/util.js");
Page({
  data: {
    //日历选择
    multiArray: [],
    //日历底标
    multiIndex: [0, 0],
    //历史数据
    datas: '',
    //月份
    months: [],
    //天数31
    days: [],
    //天数30
    day: [],
    //天数 28
    dday: [],
    //显示日历
    showtext: ''
  },
  //事件处理函数
  onLoad: function () {
    this.setData({
      showtext: util.formatTime()[0] + '月' + util.formatTime()[1] + '日',
    })
    //获取历史信息
    this.getinfor(util.formatTime()[0], util.formatTime()[1])

  },
  onReady: function () {
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex,
      months: this.data.months,
      days: this.data.days,
      day: this.data.day,
      dday: this.data.dday
    };
    data.months = util.getnum(0),
      data.days = util.getnum(1),
      data.day = util.getnum(2),
      data.dday = util.getnum(3),
      data.multiArray[0] = data.months;
    data.multiArray[1] = data.days;

    this.setData(data);

  },
  //提交的日期
  bindMultiPickerChange:function(e){
    var month=e.detail.value[0]+1
    var day=e.detail.value[1]+1//value中是选择的日期的下标值，从0开始
    this.setData({
      multiIndex:e.detail.value,
      showtext:month+'月'+day+'日'
    })
    this.getinfor(month,day)
    wx.showToast({
      title:'正在加载',
      icon:'lodaing',
      duration:1000
    })
  },
//选择日期
bindMultiPickerColumnChange:function(e){
  var data={
    multiArray:this.multiArray,
    multiIndex:this.multiIndex
  };
  data.multiIndex[e.detail.column]=e.detail.value;
  switch(e.detail.column){
    case 0:
      switch(data.multiIndex[0]+1){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
          data.multiArray[1]=this.data.days;//31天
          break;
        case 4:
        case 6:
        case 9:
        case 11:
          data.multiArray[1]=this.data.day;//30天
          break;
      }
      data.multiIndex[1]=0;
      data.multiIndex[2]=0;
      break;
  }
  this.setData(data);
},


getinfor:function(month,day){
  var that=this;
  wx.request({
    url: 'http://api.juheapi.com/japi/toh?key=cdd41437e5c2778c9ceaeb0a50cafc18&v=1.0&month='+month+'&day='+day,
    data: {
      month:month,
      day:day
    },
    method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    header:{
      'content-type':'application/json'
    },
    success: function(res){
      console.log(res.data.result);
      that.setData({
        datas:res.data.result
      })
    }
    
  })
},
showDetail:function(event){
  let {id}=event.currentTarget.dataset;
  wx.navigateTo({
    url:'/pages/detail/detail'+"?id="+id
  });
}
})
